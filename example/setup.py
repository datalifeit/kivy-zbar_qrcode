#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from distutils.core import setup
from setuptools import find_packages
import os
import io
import sys
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
logger.addHandler(ch)

opts = {}
args_ = ('--android-api', '--storage-dir', '--dist-name', '--arch',
    '--local_recipes')
for par in sys.argv:
    for arg_ in args_:
        if par.startswith(arg_):
            opts[arg_] = par[len(arg_) + 1:]

options = {
    'apk': {
        'permissions': ['CAMERA'],
        'presplash-color': 'white',
        'no-compile-pyo': None}}

if '--local-recipes' not in opts.keys():
    local_recipes_ = '../recipe'
    options['apk']['local-recipes'] = local_recipes_
    logger.info('... Local recipes: %s' % local_recipes_)

if '--android-api' in opts.keys():
    api_ = opts.get('--android-api')
else:
    api_ = os.environ.get('ANDROIDAPI') or '21'
    options['apk']['android-api'] = api_

if '--storage-dir' not in opts.keys():
    if 'P4A_STORAGE_DIR' in os.environ.keys():
        options['apk']['storage-dir'] = os.environ.get('P4A_STORAGE_DIR')
        logger.info('... Storage dir: %s' % options['apk']['storage-dir'])

if '--arch' in opts.keys():
    arch_ = opts.get('arch')
else:
    arch_ = 'armeabi-v7a'
    options['apk']['arch'] = arch_

if '--dist-name' not in opts.keys():
    _, project = os.path.split(os.getcwd())
    distribution = '%s_%s_%s' % (project, arch_, api_)
    options['apk']['dist-name'] = distribution
    logger.info('... Using/creating distribution: %s' % distribution)

if '--add-jar' not in opts.keys():
    _basepath = os.path.split(os.getcwd())[0]
    _path = os.path.join(_basepath, 'libs', 'android', 'zbar.jar')
    options['apk']['add-jar'] = _path
    logger.info('... Add jar files from: %s' % _path)


def read(fname):
    return io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()


_pack_data = {
    'qrcodeexample': [
        '*.py'],
    'res': []}


args = {}

setup(name='qrcodeexample',
    version='1.0',
    description='QR code reader app',
    author='Datalife',
    author_email='info@datalifeit.es',
    url='https://gitlab.com/datalifeit/kivy-zbar_qrcode',
    license='GPL-3',
    options=options,
    packages=['qrcodeexample'],
    package_data=_pack_data,
    install_requires=['kivy'],
    **args)
