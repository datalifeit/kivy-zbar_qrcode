Android/Kivy Qrcode scanner
===========================

This python package is based on Mathieu Virbel [android-zbar-qrcode](https://github.com/tito/android-zbar-qrcode) example project:



Featuring:

- Android camera initialization
- Show the android camera into a Android surface that act as an overlay
- New AndroidWidgetHolder that control any android view as an overlay
- New ZbarQrcodeDetector that use AndroidCamera / PreviewFrame + zbar to
  detect Qrcode.

Usage
-------

	def on_symbol(detector, symbols):
		print 'found', len(symbols), 'symbols'
		for symbol in symbols:
			print '- qrcode: {}'.format(symbol.data)

		# stop the detector if we found a symbol.
		# don't if you want continuous detection.
		detector.stop()

	detector = ZbarQrcodeDetector()
	detector.bind(symbols=on_symbols)


Compile Example:
-----------------

Go to example folder and:

	python setup.py apk

With Buildozer: modify buildozer.spec (p4a.local_recipes)
	
    buildozer android_new debug

Install in device with adb:

	adb install <apk_file>